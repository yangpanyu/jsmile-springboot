$(function(){
  var role={
     init:function(){
    	 var _this=this;
    	 _this.initDatagrid();
    	 _this.initEvent();
     },
     initEvent:function(){
	    var _this=this;
		//角色权限实现
		$("#role_resource").click(function(){
 			var selRows=_this.roleDataGrid.datagrid('getChecked');
 			JSmile.KEasy.edit(selRows,function(){
     	        top.$.modalDialog({
                    width:300,
                    height:400,
     	            title : '角色权限修改',
     	            href:"/sys_role/resPage?roleId="+selRows[0].id,
            		buttons:[{text:'确定',handler:function(){
        			   var resources_tree=top.$.modalDialog.handler.find(top.$("#resources_tree"));
        	           var sels=resources_tree.tree('getChecked');
        	           var resourcesIds=[];
        	           for(var i=0;i<sels.length;i++){
        	        	   resourcesIds.push(sels[i].id);
        	           }
	     	           	JSmile.KHttp.send({
     	           		url:"/sys_role/saveRoleResources",
     	       		    data:{"roleId":selRows[0].id,"resourcesIds":resourcesIds}}).done(function(res){
     	       		        top.$.messager.alert("提示", res.resMsg, "info"); 
     	       		    	top.$.modalDialog.handler.dialog('close');
	     	       	    });
        	    	}},{text:'取消',handler:function(){
        	    			top.$.modalDialog.handler.dialog('close');
        	    	}}]
     	        }).dialog('open');
 			});
		});
 		//添加实现
 		$("#add_role").click(function(){
 	        top.$.modalDialog({
                _form:"#roleForm", 
                _datagrid:_this.roleDataGrid,
 	            title : '角色信息添加',
 	            href:"/sys_role/updatePage",
 	            onLoad:function(){
 	            	var f=top.$.modalDialog.handler.find("#roleForm"); 
 	            	f.form("reset");
 	            },
 	            onCloseAfter: function(){
                    if(top.UE.getEditor('editor')){
                    	top.UE.getEditor('editor').destroy();
                    }
 	            }
 	        }).dialog('open');
 		});
 		//删除实现
 		$("#del_role").click(function(){
 			var selRows=_this.roleDataGrid.datagrid('getSelections');
 			JSmile.KEasy.del('/sys_role/deleteByIds',selRows,function(res){
 				_this.roleDataGrid.datagrid('clearChecked').datagrid('reload');
 			});
 		});
 		$("#edit_role").click(function(){
 			var selRows=_this.roleDataGrid.datagrid('getChecked');
 			JSmile.KEasy.edit(selRows,function(){
 	 	        top.$.modalDialog({
 	                _form:"#roleForm", 
 	                _datagrid:_this.roleDataGrid, 	 	        	
 	 	            title : '角色信息修改',
 	 	            href:"/sys_role/updatePage",
 	 	            onLoad:function(){
 	 	            	var f=top.$.modalDialog.handler.find("#roleForm"); 
 	 	            	f.form("reset");
 	 	            	f.form("load",selRows[0]);
 	 	            }
 	 	        }).dialog('open');
 			});
 		});
 		//搜索实现
 		$("#search_role").click(function(){
 			_this.roleDataGrid.datagrid('load',$('#roleserSearchForm').serializeJson());
 		}); 
     },
     initDatagrid:function(){//初始化各种datagrid
    	var _this=this;
    	_this.roleDataGrid=$('#role_datagrid').datagrid(JSmile.KEasy.initDatagrid({
	    	url:'/sys_role/findAllByPage',
	    	toolbar:'#role_toolbar',
	    	columns:[[
	   		 {field:'id',hidden:true},
			 {field:'ck',checkbox:true},
			 {field:'roleName',title:'角色名称',align:'center',width:120},
			 {field:'roleCode',title:'角色编码',align:'center',width:120},
			 {field:'createDate',title:'时间',align:'center',width:120,sortable:true},
			 {field:'remark',title:'备注',align:'center',width:120},
			 {field:'status',title:'状态',align:'center',width:80,sortable:true,formatter:JSmile.KStatus.initStatus}
		]]
	    }));
     }
  };
  role.init();
});