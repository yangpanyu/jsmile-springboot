$(function(){
  var dictionary={
     init:function(){
    	 var _this=this;
    	 _this.initTreegrid();
    	 _this.initEvent();
     },
     initEvent:function(){
    	 var _this=this;
 		//添加实现
 		$("#add_dictionary").click(function(){
 	        top.$.modalDialog({
                _form:"#dictionaryForm", 
                _treegrid:_this.dictionaryTreeGrid,
 	            title : '添加数据字典',
 	            href:"/sys_dictionary/updatePage",
 	            onLoad:function(){
 	            	var f=top.$.modalDialog.handler.find("#dictionaryForm"); 
 	            	f.form("reset");
 	            }
 	        }).dialog('open');
 		});
 		//删除实现
 		$("#del_dictionary").click(function(){
 			var selRows=_this.dictionaryTreeGrid.treegrid('getSelections');
 			JSmile.KEasy.del('/sys_dictionary/deleteByIds',selRows,function(res){
 				_this.dictionaryTreeGrid.treegrid('clearChecked').treegrid('reload');
 			});
 		});
 		$("#edit_dictionary").click(function(){
 			var selRows=_this.dictionaryTreeGrid.treegrid('getChecked');
 			JSmile.KEasy.edit(selRows,function(){
 	 	        top.$.modalDialog({
 	                _form:"#dictionaryForm", 
 	                _treegrid:_this.dictionaryTreeGrid, 	 	        	
 	 	            title : '字典信息修改',
 	 	            href:"/sys_dictionary/updatePage",
 	 	            onLoad:function(){
 	 	            	var f=top.$.modalDialog.handler.find("#dictionaryForm"); 
 	 	            	f.form("reset");
 	 	            	f.form("load",selRows[0]);
 	 	            }
 	 	    }).dialog('open');
 			});
 		});
 		//搜索实现
 		$("#search_dictionary").click(function(){
 			_this.dictionaryTreeGrid.treegrid('load',$('#dictionarySearchForm').serializeJson());
 		}); 
     },
     initTreegrid:function(){//初始化各种treegrid
    	var _this=this;
    	_this.dictionaryTreeGrid=$('#dictionary_treegrid').treegrid(JSmile.KEasy.initTreegrid({
	    	url:'/sys_dictionary/findAll',
	    	toolbar:'#dictionary_toolbar',
	    	treeField:"label",
	    	loadFilter:function(data,parentId){
	    		var d={};
	    		d.rows=JSmile.convertTreegrid(data,{textField:"label"});
	    		d.total=data.resData.total;
	    		return d;
	    	},
	    	columns:[[
	   		 {field:'id',hidden:true},
			 {field:'label',title:'标签',align:'left',width:100},
			 {field:'value',title:'值',align:'center',width:100},
			 {field:'type',title:'类型',align:'center',width:100},
			 {field:'status',title:'状态',align:'center',width:80,sortable:true,formatter:JSmile.KStatus.initStatus},
			 {field:'remark',title:'备注',align:'center',width:120}
		]]
	    }));
     }
  };
  dictionary.init();
});