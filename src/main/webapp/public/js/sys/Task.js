$(function(){
  var task={
     init:function(){
    	 var _this=this;
    	 _this.initDatagrid();
    	 _this.initEvent();
     },
     initEvent:function(){
	    var _this=this;
 		//删除实现
 		$("#del_task").click(function(){
 			var selRows=_this.taskDataGrid.datagrid('getSelections');
 			JSmile.KEasy.del('/sys_task/deleteByIds',selRows,function(res){
 				_this.userDataGrid.datagrid('clearChecked').datagrid('reload');
 			});
 		});
 		//添加实现
 		$("#add_task").click(function(){
 	        top.$.modalDialog({
                _form:"#taskForm", 
                _datagrid:_this.taskDataGrid,
 	            title : '用户信息添加',
 	            href:"/sys_task/updatePage",
 	            onLoad:function(){
 	            	var f=top.$.modalDialog.handler.find("#taskForm"); 
 	            	f.form("reset");
 	            }
 	        }).dialog('open');
 		});
 		$("#edit_task").click(function(){
 			var selRows=_this.taskDataGrid.datagrid('getChecked');
 			JSmile.KEasy.edit(selRows,function(){
 	 	        top.$.modalDialog({
 	                _form:"#taskForm", 
 	                _datagrid:_this.taskDataGrid, 	 	        	
 	 	            title : '用户信息修改',
 	 	            href:"/sys_task/updatePage",
 	 	            onLoad:function(){
 	 	            	var f=top.$.modalDialog.handler.find("#taskForm"); 
 	 	            	f.form("reset");
 	 	            	f.form("load",selRows[0]);
 	 	            }
 	 	        }).dialog('open');
 			});
 		});
     },
     initDatagrid:function(){//初始化各种datagrid
    	var _this=this;
    	_this.taskDataGrid=$('#task_datagrid').datagrid(JSmile.KEasy.initDatagrid({
	    	url:'/sys_task/findAllByPage',
	    	toolbar:'#task_toolbar',
	    	columns:[[
	   		 {field:'id',hidden:true},
			 {field:'ck',checkbox:true},
			 {field:'jobName',title:'任务名称',align:'center',width:100},
			 {field:'jobGroup',title:'任务分组',align:'center',width:100},
			 {field:'createDate',title:'创建时间',align:'center',width:100,sortable:true},
			 {field:'cron',title:'表达式',align:'center',width:100},
			 {field:'beanClass',title:'包名+类名',align:'center',width:100},
			 {field:'methodName',title:'执行方法',align:'center',width:100},
			 {field:'status',title:'状态',align:'center',width:80,sortable:true,formatter:function(value,row,index){
				if(value==1||value=='1'||value==2){
					return "<div style=\"background-color:#5cb85c;width:60%;color:white;margin:0 auto;\">正常</div>";
				}else if(value=='0'||value==0){
					return "<div style=\"background-color:#d9534f;width:60%;color:white;margin:0 auto;\">禁用</div>";
				}else if(value=='4'||value==4){
					return "<div style=\"background-color:#d9534f;width:60%;color:white;margin:0 auto;\">停止</div>";
				}else if(value=='3'||value==3){
					return "<div style=\"background-color:#d9534f;width:60%;color:white;margin:0 auto;\">暂停</div>";
				}
			 }},
			 {field:'remark',title:'备注',align:'center',width:100},
			 {field:'action',title:'操作',align:'center',width:100,formatter: function(value,row,index){
				 if(row.status=='0'){
					 var tem=JSmile.Kit.formatString('<a class="easyui-tooltip" style="cursor:pointer;padding:3px;" title=\"启用\" onclick="updateTaskStatus(\'{0}\',\'{1}\');" ><i style="color:#5eb95e !important" class="iconfont sn-enable"></i></a>', row.id,1);
					 return tem+=JSmile.Kit.formatString('<a class="easyui-tooltip" style="cursor:pointer;padding:3px;" title=\"删除\" onclick="deleteTask(\'{0}\');" ><i style="color:#cc2222 !important" class="iconfont sn-remove"></i></a>', row.id);
				 }else if(row.status=='1'||row.status=='2'||row.status=='4'){
					 var tem=JSmile.Kit.formatString('<a class="easyui-tooltip" style="cursor:pointer;padding:3px;" title=\"立即执行\" onclick="updateTaskStatus(\'{0}\',\'{1}\');" ><i style="color:#5eb95e !important" class="iconfont sn-run"></i></a>', row.id,2);
					 tem+=JSmile.Kit.formatString('<a class="easyui-tooltip" style="cursor:pointer;padding:3px;" title=\"停止\" onclick="updateTaskStatus(\'{0}\',\'{1}\');" ><i style="color:#dd514c !important" class="iconfont sn-cease"></i></a>', row.id,0);
					 tem+=JSmile.Kit.formatString('<a class="easyui-tooltip" style="cursor:pointer;padding:3px;" title=\"暂停\" onclick="updateTaskStatus(\'{0}\',\'{1}\');" ><i style="color:##F37B1D !important" class="iconfont sn-stop"></i></a>', row.id,3);
					 return tem;
				 }else if(row.status=='3'){//暂停
					 return JSmile.Kit.formatString('<a class="easyui-tooltip" style="cursor:pointer;padding:3px;" title=\"恢复\" onclick="updateTaskStatus(\'{0}\',\'{1}\');" ><i style="color:#5eb95e !important" class="iconfont sn-huifu"></i></a>', row.id,4);
				 }
			 }}
		]]
	    }));
     }
  };
  task.init();
});
/*
 * 删除任务
 */
function deleteTask(id){
	top.$.messager.alert("任务", "确认删除该任务!", "info",function(){
		JSmile.KHttp.send({
			url:"/sys_task/deleteById",
			data:{"id":id}
		}).done(function(){
			$('#task_datagrid').datagrid('reload');
		});
	});  
}
/*
 * 更改任务状态
 */
function updateTaskStatus(id,status){
	if("0"==status){
		top.$.messager.alert("任务", "确认停止该任务!", "info",function(){
			changeTaskStatus(id,status);
		});  
	}else if("1"==status){
		top.$.messager.alert("任务", "确认启用该任务!", "info",function(){
			changeTaskStatus(id,status);
		});  
	}else if("2"==status){
		top.$.messager.alert("任务", "确认立即执行该任务!", "info",function(){
			changeTaskStatus(id,status);
		});  
	}else if("3"==status){
		top.$.messager.alert("任务", "确认暂停该任务!", "info",function(){
			changeTaskStatus(id,status);
		});  
	}else if("4"==status){
		top.$.messager.alert("任务", "确认恢复该任务!", "info",function(){
			changeTaskStatus(id,status);
		});  
	}
}
function changeTaskStatus(id,status){
	JSmile.KHttp.send({
		url:"/sys_task/changeJobStatus",
		data:{"id":id,"status":status}
	}).done(function(){
		$('#task_datagrid').datagrid('reload');
	});
}