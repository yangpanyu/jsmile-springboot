$(function(){
  var dept={
     init:function(){
    	 var _this=this;
    	 _this.initTreegrid();
    	 _this.initEvent();
     },
     initEvent:function(){
    	 var _this=this;
 		//添加实现
 		$("#add_dept").click(function(){
 	        top.$.modalDialog({
                _form:"#deptForm", 
                _treegrid:_this.deptTreeGrid,
 	            title : '添加部门',
 	            href:"/sys_dept/updatePage",
 	            onLoad:function(){
 	            	var f=top.$.modalDialog.handler.find("#deptForm"); 
 	            	f.form("reset");
 	            }
 	        }).dialog('open');
 		});
 		//删除实现
 		$("#del_dept").click(function(){
 			var selRows=_this.deptTreeGrid.treegrid('getSelections');
 			JSmile.KEasy.del('/sys_dept/deleteByIds',selRows,function(res){
 				_this.deptTreeGrid.treegrid('clearChecked').treegrid('reload');
 			});
 		});
 		$("#edit_dept").click(function(){
 			var selRows=_this.deptTreeGrid.treegrid('getChecked');
 			JSmile.KEasy.edit(selRows,function(){
 	 	        top.$.modalDialog({
 	                _form:"#deptForm", 
 	                _treegrid:_this.deptTreeGrid, 	 	        	
 	 	            title : '部门信息修改',
 	 	            href:"/sys_dept/updatePage",
 	 	            onLoad:function(){
 	 	            	var f=top.$.modalDialog.handler.find("#deptForm"); 
 	 	            	f.form("reset");
 	 	            	f.form("load",selRows[0]);
 	 	            }
 	 	    }).dialog('open');
 			});
 		});
     },
     initTreegrid:function(){//初始化各种treegrid
    	var _this=this;
    	_this.deptTreeGrid=$('#dept_treegrid').treegrid(JSmile.KEasy.initTreegrid({
	    	url:'/sys_dept/findAll',
	    	toolbar:'#dept_toolbar',
	    	treeField:"deptName",
	    	loadFilter:function(data,parentId){
	    		var d={};
	    		d.rows=JSmile.convertTreegrid(data,{textField:"deptName"});
	    		d.total=data.resData.total;
	    		return d;
	    	},
	    	columns:[[
	   		 {field:'id',hidden:true},
			 {field:'deptName',title:'部门名称',align:'left',width:100},
			 {field:'deptAddress',title:'部门地址',align:'center',width:120},
			 {field:'deptPhone',title:'部门电话',align:'center',width:100},
			 {field:'type',title:'类型',align:'center',width:80,formatter:function(value,row,index){
				 if(1==value)return "公司";else if(2==value)return "公司领导";else if(3==value)return "部门";else return "其它";
			 }},
			 {field:'createDate',title:'时间',align:'center',width:120,sortable:true},
			 {field:'remark',title:'备注',align:'center',width:120}
		]]
	    }));
     }
  };
  dept.init();
});