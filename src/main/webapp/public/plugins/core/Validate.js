/******************扩展validate验证***********************/
$.extend($.fn.validatebox.defaults.rules, {  
    //验证汉字  
    CHS: {  
        validator: function (value) {  
            return JSmile.Kit.isChinese(value);  
        },  
        message: '请输入汉字!'  
    },  
    //移动手机号码验证  
    Mobile: {//value值为文本框中的值  
        validator: function (value) {  
        	return JSmile.Kit.isMobile(value); 
        },  
        message: '请输入手机号码!'  
    },  
    Tel: {//value值为文本框中的值  
        validator: function (value) {  
        	return JSmile.Kit.isTel(value); 
        },  
        message: '请输入固定电话!'  
    }, 
    Pwd:{ //  密码中必须包含字母、数字、特称字符，至少6个字符
    	validator: function(value){
    		return /^[a-zA-Z][a-zA-Z0-9_?.*!@#$%^&]{6,16}$/i.test(value);
    	},
    	message: '密码中必须包含字母、数字、特称字符，至少6个字符!'
    },
    //国内邮编验证  
    ZipCode: {  
        validator: function (value) {  
            return JSmile.Kit.isZipCode(value); 
        },  
        message: '请输入正确的邮编!'  
    },  
  //数字  
    Number: {  
        validator: function (value) {  
            return JSmile.Kit.isNumber(value); 
        },  
        message: '请输入数字!'  
    },
    //字母和: 
    PermCode: {  
        validator: function (value) {  
            var reg =/^(\[a-zA-Z]+:)*(\[a-zA-Z])+$/;  
            return reg.test(value);  
        },  
        message: '只能为:和数字!'  
    },
    //能为数字或小数点 
    Version: {  
        validator: function (value) {  
            var reg =/^(\d+.)*(\d)+$/;  
            return reg.test(value);  
        },  
        message: '只能为数字和小数点!'  
    },
    //  验证是否可以作为合法的用户名字符(字母开头，允许6-16字节，允许字母数字下划线)
    UserName:{
    	validator: function(value){
    		return JSmile.Kit.isUserName(value); 
    	},
    	message: '请输入合法的用户名!'
    },
    //  验证是否身份证
    IdCard:{
    	validator: function(value){
    		return JSmile.Kit.isIDCard(value); 
    	},
    	message: '请输入合法的身份证!'
    },
    //  验证是否英文
    English:{
    	validator: function(value){
    		return JSmile.Kit.isEnglish(value); 
    	},
    	message: '请输入英文!'
    },
    //  验证是否邮箱
    Email:{
    	validator: function(value){
    		return JSmile.Kit.isEmail(value); 
    	},
    	message: '请输入正确邮箱!'
    },
    EqPwd : {
        validator : function(value, param) {
            return value == $(param[0]).val();
        },
        message : '密码不一致！'
    },
    CheckDate:{
        validator : function(value) {
        	  var nowDate = new Date();  
              var dateList = value.split("/");  
              var chooseData = new Date(dateList[2],dateList[0]-1,dateList[1]);   
              return nowDate>chooseData;  
        },
        message : '时间必须大于当前时间'
    },
    CheckCombobox:{
        validator : function(value) {
              return !(value==="--请选择--");  
        },
        message : '该项为必须选择项!'
    }
});