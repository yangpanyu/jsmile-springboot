package org.gongliang.task;

import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import org.gongliang.entity.sys.Log;
import org.gongliang.kit.web.SpringKit;
import org.gongliang.service.sys.LogService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Lists;

/**
 * 日志记录任务
 * 
 */
public class TaskLog {

	private static final Logger logger = LoggerFactory.getLogger(TaskLog.class);

	private volatile static TaskLog singleton;

	public TaskLog() {
	}

	/**
	 * 获取日志单例
	 * 
	 * @return
	 */
	public static TaskLog getSingleton() {
		if (singleton == null) {
			synchronized (TaskLog.class) {
				if (singleton == null) {
					singleton = new TaskLog();
				}
			}
		}
		return singleton;
	}

	/**
	 * 日志记录队列
	 */
	private static BlockingQueue<Log> queueLog = new ArrayBlockingQueue<Log>(
			1000);

	private static LogService logService = SpringKit.getBean(LogService.class);// 日志服务

	/**
	 * 添加记录
	 * 
	 * @param o
	 */
	public void add(Log log) {
		queueLog.offer(log);
		logger.info("记录了一条日志：" + log.getReqUrl());
	}

	/**
	 * 将日志入库任务
	 */
	public void run() {
		if (queueLog.size() > 0)
			logger.info("当前日志队列总数：" + queueLog.size());
		int i = 0;
		List<Log> logs = Lists.newArrayList();// 临时日志堆栈
		while (queueLog.size() > 0) {
			if (i > 1000) {
				break;
			}
			Log log = queueLog.poll();
			logs.add(log);
			i++;
		}
		if (logs.size() > 0) {
			logService.saveBatch(logs);
			logger.info("当前日志入库队列数：" + logs.size());
		}

	}

}
