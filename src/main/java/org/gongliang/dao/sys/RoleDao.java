package org.gongliang.dao.sys;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.gongliang.common.base.BaseDao;
import org.gongliang.entity.sys.Resources;
import org.gongliang.entity.sys.Role;

/**
* 
* @author 龚亮
* @version 1.0
* @date 2017年05月07日 05:14:22
*/
public interface RoleDao extends BaseDao<Role> {

	/**
	 * 获取用户所拥有的角色
	 * 
	 * @param userId
	 *            用户id
	 * @return 角色列表
	 */
	@Select("SELECT  r.*  FROM sys_role r LEFT JOIN sys_user_role ur ON  r.id=ur.role_id WHERE ur.user_id=#{userId}")
	List<Role> getUserRole(Long userId);

	@Insert("insert into sys_user_role values(#{userId},#{roleId})")
	int saveUserRole(@Param("userId") Long userId, @Param("roleId") Long roleId);

	@Delete("delete from sys_user_role where user_id=#{userId}")
	int deleteUserRole(@Param("userId") Long userId);

	/**
	 * 获取角色所拥有的权限
	 * 
	 * @param roleId
	 *            角色id
	 * @return 资源列表
	 */
	@Select("SELECT resources_id id from sys_role_resources where role_id=#{roleId}")
	List<Resources> getRoleResource(Long roleId);

	@Delete("delete from sys_role_resources where role_id=#{roleId}")
	int deleteRoleResources(Long roleId);

	@Insert("insert into sys_role_resources values(#{roleId},#{resourcesId})")
	int saveRoleResources(@Param("roleId") Long roleId, @Param("resourcesId") Long resourcesId);
}
