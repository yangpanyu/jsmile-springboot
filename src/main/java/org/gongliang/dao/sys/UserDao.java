package org.gongliang.dao.sys;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.gongliang.common.base.BaseDao;
import org.gongliang.entity.sys.User;

/**
* 
* @author 龚亮
* @version 1.0
* @date 2017年05月07日 05:14:22
*/
public interface UserDao extends BaseDao<User> {

	@Update("UPDATE sys_user SET password=#{newPassword} WHERE id=#{userId} AND password=#{password}")
	Integer updatePassword(@Param("userId") Long userId, @Param("password") String password,
			@Param("newPassword") String newPassword);

	@Select("SELECT * FROM sys_user where username=#{username} limit 1")
	User findUser(@Param("username") String username);
}
