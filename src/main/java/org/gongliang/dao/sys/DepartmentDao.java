package org.gongliang.dao.sys;

import org.gongliang.common.base.BaseDao;
import org.gongliang.entity.sys.Department;

/**
* 
* @author 龚亮
* @version 1.0
* @date 2017年05月07日 05:14:22
*/
public interface DepartmentDao extends BaseDao<Department> {

}
