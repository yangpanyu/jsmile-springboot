package org.gongliang.dao.sys;

import java.util.List;

import org.gongliang.common.base.BaseDao;
import org.gongliang.entity.sys.Log;

/**
* 
* @author 龚亮
* @version 1.0
* @date 2017年05月07日 05:14:22
*/
public interface LogDao extends BaseDao<Log> {

	public int saveBatch(List<Log> logs);
}
