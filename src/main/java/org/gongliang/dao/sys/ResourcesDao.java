package org.gongliang.dao.sys;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.gongliang.common.base.BaseDao;
import org.gongliang.entity.sys.Resources;

/**
* 
* @author 龚亮
* @version 1.0
* @date 2017年05月07日 05:14:22
*/
public interface ResourcesDao extends BaseDao<Resources> {

	/**
	 * 获取用户所拥有的菜单或权限
	 * 
	 * @param userId
	 *            用户id
	 * @param type
	 *            1菜单 2权限 -1全部
	 * @return 权限或菜单列表
	 */
	List<Resources> getUserPermission(@Param("userId") Long userId, @Param("type") String type);
}
