package org.gongliang.common.base;

import java.util.List;
import java.util.Map;

public interface BaseDao<T> {

	/*
	 * 方法描述: 根据columnMap获取所有数据 方法返回: List<Entity>
	 */
	public List<T> findAll(Map<String, Object> columnMap);

	/*
	 * 方法描述: 根据columnMap获取所有数据 方法返回: List<Map<String, Object>>
	 */
	public List<Map<String, Object>> findAllMap(Map<String, Object> columnMap);

	/*
	 * 方法描述: 根据Id查询单条记录 方法返回: Entity
	 */
	public T findById(String id);

	/**
	 * 保存数据实体entity
	 */
	public Integer save(T entity);

	/**
	 * 更新数据Entity
	 */
	public Integer updateById(T entity);

	/**
	 * 根据id删除数据
	 */
	public Integer deleteById(String id);

	/**
	 * 根据id批量删除
	 * 
	 * @param ids
	 *            id数组
	 */
	public Integer deleteByIds(String[] ids);

}