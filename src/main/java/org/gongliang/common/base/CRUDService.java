package org.gongliang.common.base;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

public abstract class CRUDService<M extends BaseDao<T>, T> implements BaseService<T> {

	@Autowired
	protected M baseDao;

	public static boolean retBool(Integer result) {
		return (null == result) ? false : result >= 1;
	}

	@Override
	@Transactional(readOnly = true, rollbackFor = Exception.class)
	public List<T> findAll(Map<String, Object> columnMap) {
		return baseDao.findAll(columnMap);
	}

	@Override
	@Transactional(readOnly = true, rollbackFor = Exception.class)
	public List<Map<String, Object>> findAllMap(Map<String, Object> columnMap) {
		return baseDao.findAllMap(columnMap);
	}

	@Override
	@Transactional(readOnly = true, rollbackFor = Exception.class)
	public T findById(String id) {
		return baseDao.findById(id);
	}


	@Override
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public boolean save(T entity) {
		return retBool(baseDao.save(entity));
	}

	@Override
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public boolean updateById(T entity) {
		return retBool(baseDao.updateById(entity));
	}


	@Override
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public boolean deleteById(String id) {
		return retBool(baseDao.deleteById(id));
	}

	@Override
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public boolean deleteByIds(String[] ids) {
		return retBool(baseDao.deleteByIds(ids));
	}
}
