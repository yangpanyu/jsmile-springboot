package org.gongliang.common;

import org.gongliang.kit.web.SpringKit;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
/**
 * 
 * @Description 全局配置文件
 * @author 龚亮
 * @date 2017年3月30日 下午3:34:16
 */
@ConfigurationProperties(prefix = "jsmile")
@Component
public class KConfig {

	public static KConfig me() {
		return SpringKit.getBean(KConfig.class);
	}
	private String fileUploadPath;
	private String fileDownPath;

	public String getFileUploadPath() {
		return fileUploadPath;
	}

	public void setFileUploadPath(String fileUploadPath) {
		this.fileUploadPath = fileUploadPath;
	}

	public String getFileDownPath() {
		return fileDownPath;
	}

	public void setFileDownPath(String fileDownPath) {
		this.fileDownPath = fileDownPath;
	}
}
