package org.gongliang.common.mybatis;

import java.util.Date;
import java.util.HashMap;

import org.gongliang.kit.DateKit;
import org.gongliang.kit.str.StrKit;

public class MybatisMap extends HashMap<String, Object>{

	private static final long serialVersionUID = -8052486001634717257L;

	@Override
	public Object get(Object key) {
		return super.get(key);
	}

	@Override
	public Object put(String key, Object value) {
		key = StrKit.underlineToCamelhump(key);
		if (value instanceof Date) {
			value = DateKit.formatDate((Date) value, "yyyy-MM-dd hh:mm:ss");
		}
		return super.put(key, value);
	}
}
