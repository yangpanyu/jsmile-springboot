package org.gongliang.common.mybatis;

import java.lang.reflect.Field;

import org.gongliang.common.annotation.TableField;
import org.gongliang.common.annotation.TableName;
import org.gongliang.entity.sys.User;

import jodd.bean.BeanUtil;

public class SqlBuilder {

	/*
	 * 获取实体类主键
	 */
	public static String getPrimaryKey(Object parameterObject) {
		String primarykey = "id";
		Field[] fields = parameterObject.getClass().getDeclaredFields();
		for (int i = 0; i < fields.length; i++) {
			Field field = fields[i];
			if (field.isAnnotationPresent(TableField.class)
					&& field.getAnnotation(TableField.class).exist()) {
				TableField tableField = field.getAnnotation(TableField.class);
				if (tableField.isPrimaryKey()) {
					primarykey = field.getName();
					break;
				}
			}
		}
		return primarykey;
	}

	/**
	 * 构建insert语句
	 * 
	 * @param parameterObject
	 * @return sql
	 */
	public static String buildInsert(Object parameterObject) {
		StringBuffer sb = new StringBuffer();
		String tablename = parameterObject.getClass()
				.getAnnotation(TableName.class).value();
		Field[] fields = parameterObject.getClass().getDeclaredFields();
		sb.append("INSERT INTO " + tablename);
		String columns = "(";
		String columnsValue = "VALUES(";
		for (int i = 0; i < fields.length; i++) {
			Field field = fields[i];
			Object value = null;
			String column = null;
			if (field.isAnnotationPresent(TableField.class)
					&& field.getAnnotation(TableField.class).exist()) {
				TableField tableField = field.getAnnotation(TableField.class);
				column = tableField.value();
				value = BeanUtil.declared.getProperty(parameterObject,
						field.getName());
				if (tableField.isPrimaryKey()) {
					continue;
				}
			}
			if (notBlank(value) && notBlank(column)) {
				if (!"class java.lang.Integer"
						.equals(field.getType().toString())) {
					value = "\"" + value + "\"";
				}
				columns += column + ",";
				columnsValue += value + ",";
			}
		}
		columns = columns.substring(0,
				columns.lastIndexOf(",") == -1 ? 0 : columns.lastIndexOf(","));
		columnsValue = columnsValue.substring(0,
				columnsValue.lastIndexOf(",") == -1 ? 0
						: columnsValue.lastIndexOf(","));
		sb.append(columns + ") ");
		sb.append(columnsValue + ")");
		return sb.toString();
	}

	/**
	 * 构建更新语句
	 * 
	 * @param parameterObject
	 * @return sql
	 */
	public static String buildUpdate(Object parameterObject) {
		StringBuffer sb = new StringBuffer();
		String tablename = parameterObject.getClass()
				.getAnnotation(TableName.class).value();
		Field[] fields = parameterObject.getClass().getDeclaredFields();
		String where = "";
		sb.append("update " + tablename);
		String set = "";
		for (int i = 0; i < fields.length; i++) {
			Field field = fields[i];
			Object value = null;
			String column = null;
			if (field.isAnnotationPresent(TableField.class)
					&& field.getAnnotation(TableField.class).exist()) {
				TableField tableField = field.getAnnotation(TableField.class);
				column = tableField.value();
				value = BeanUtil.declared.getProperty(parameterObject,
						field.getName());
				if (tableField.isPrimaryKey()) {
					if ("class java.lang.Integer"
							.equals(field.getType().toString())) {
						where += " WHERE " + column + "=" + value;
					} else {
						where += " WHERE " + column + "=\"" + value + "\"";
					}
				}
				if (notBlank(value) && notBlank(column)) {
					if ("class java.lang.Integer"
							.equals(field.getType().toString())) {
						value = "=" + value + ",";
					} else {
						value = "=\"" + value + "\",";
					}
					if (!tableField.isPrimaryKey()) {
						if (set.contains("SET")) {
							set += column + value;

						} else {
							set += " SET " + column + value;
						}
					}
				}
			}
		}
		set = set.substring(0,
				set.lastIndexOf(",") == -1 ? 0 : set.lastIndexOf(","));
		sb.append(set);
		sb.append(where);
		return sb.toString();
	}

	/**
	 * 构建删除语句
	 * 
	 * @param parameterObject
	 * @return sql
	 */
	public static String buildDelete(Object parameterObject) {
		StringBuffer sb = new StringBuffer();
		String tablename = parameterObject.getClass()
				.getAnnotation(TableName.class).value();
		Field[] fields = parameterObject.getClass().getDeclaredFields();
		sb.append("DELETE FROM " + tablename);
		String where = " ";
		for (int i = 0; i < fields.length; i++) {
			Field field = fields[i];
			Object value = null;
			String column = null;
			if (field.isAnnotationPresent(TableField.class)
					&& field.getAnnotation(TableField.class).exist()) {
				TableField tableField = field.getAnnotation(TableField.class);
				column = tableField.value();
				value = BeanUtil.declared.getProperty(parameterObject,
						field.getName());
			}
			if (notBlank(value) && notBlank(column)) {
				if ("class java.lang.Integer"
						.equals(field.getType().toString())) {
					value = "=" + value;
				} else {
					value = "=\"" + value + "\"";
				}
				if (where.contains("WHERE")) {
					where += " AND " + column + value;
				} else {
					where += " WHERE " + column + value;
				}
			}
		}
		sb.append(where);
		return sb.toString();
	}

	public static String buildQuery(Object parameterObject) {
		StringBuffer sb = new StringBuffer();
		String tablename = parameterObject.getClass()
				.getAnnotation(TableName.class).value();
		Field[] fields = parameterObject.getClass().getDeclaredFields();
		sb.append("SELECT * FROM " + tablename);
		String where = " ";
		for (int i = 0; i < fields.length; i++) {
			Field field = fields[i];
			Object value = null;
			String column = null;
			if (field.isAnnotationPresent(TableField.class)
					&& field.getAnnotation(TableField.class).exist()) {
				TableField tableField = field.getAnnotation(TableField.class);
				column = tableField.value();
				value = BeanUtil.declared.getProperty(parameterObject,
						field.getName());
			}
			if (notBlank(value) && notBlank(column)) {
				if ("class java.lang.Integer"
						.equals(field.getType().toString())) {
					value = "=" + value;
				} else {
					value = "=\"" + value + "\"";
				}
				if (where.contains("WHERE")) {
					where += " AND " + column + value;
				} else {
					where += " WHERE " + column + value;
				}
			}
		}
		sb.append(where);
		return sb.toString() + " LIMIT 1";
	}

	public static boolean notBlank(Object str) {
		if (null == str)
			return false;
		return ((str != null) && (!("".equals(str.toString().trim()))));
	}

	public static void main(String[] args) {
		User user = new User();
		user.setUsername("test");
		user.setId(1L);
		user.setPhone("123");
		user.setUsername("test");
		System.out.println(buildDelete(user));
		System.out.println(buildInsert(user));
		System.out.println(buildUpdate(user));
		System.out.println(buildQuery(user));
		System.out.println(getPrimaryKey(user));
	}
}
