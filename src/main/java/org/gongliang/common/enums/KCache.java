package org.gongliang.common.enums;

/*
 * 缓存管理
 */
public enum KCache {

	halfHour("halfHour", 1800L), hour("hour", 3600L), oneMonth("oneMonth", 2592000L), oneDay("oneDay", 86400L), oneYear(
			"oneYear", 31536000L), userinfoCache("userinfoCache",
					1800L), permissionCache("permissionCache", 1800L), activeSessionCache("activeSessionCache", 1800L);

	private Long expires;
	private String cacheName;

	KCache(String cacheName, Long expires) {
		this.expires = expires;
		this.cacheName = cacheName;
	}

	public String getCacheName() {
		return cacheName;
	}

	public void setCacheName(String cacheName) {
		this.cacheName = cacheName;
	}


	public Long getExpires() {
		return expires;
	}

	public void setExpires(Long expires) {
		this.expires = expires;
	}
}
