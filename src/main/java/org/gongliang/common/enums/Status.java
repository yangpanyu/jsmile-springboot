package org.gongliang.common.enums;

/*
 * 状态
 */
public enum Status {

	normal(1, "正常"), delete(0, "删除"), all(-1, "全部");

	private Integer code;
	private String des;

	Status(Integer code, String des) {
		this.code = code;
		this.des = des;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getDes() {
		return des;
	}

	public void setDes(String des) {
		this.des = des;
	}

	public static String valueOf(Integer value) {
		if (value == null) {
			return "";
		} else {
			for (Status rt : Status.values()) {
				if (rt.getCode() == value) {
					return rt.getDes();
				}
			}
			return "";
		}
	}
}
