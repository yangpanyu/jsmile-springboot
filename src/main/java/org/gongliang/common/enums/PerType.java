package org.gongliang.common.enums;

/*
 * 菜单类型
 */
public enum PerType {

	male(1, "菜单"), female(2, "权限"), all(-1, "全部");

	private Integer code;
	private String des;

	PerType(Integer code, String des) {
		this.code = code;
		this.des = des;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getDes() {
		return des;
	}

	public void setDes(String des) {
		this.des = des;
	}

	public static String valueOf(Integer value) {
		if (value == null) {
			return "";
		} else {
			for (PerType rt : PerType.values()) {
				if (rt.getCode() == value) {
					return rt.getDes();
				}
			}
			return "";
		}
	}
}
