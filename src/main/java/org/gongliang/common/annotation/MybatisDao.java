package org.gongliang.common.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @description 扫描所有dao接口
 * @author 龚亮
 * @date 2015-05-26 09:51:23
 */
@Retention(RetentionPolicy.RUNTIME) 
@Target(ElementType.TYPE)
@Documented
public @interface MybatisDao {

}
