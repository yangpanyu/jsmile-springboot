package org.gongliang.common.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <p>
 * 表字段标识
 * </p>
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Documented
public @interface TableField {

	/**
	 * 数据库字段
	 */
	String value() default "";

	/**
	 * 是否为数据库表主键 默认 true 是,false 不
	 */
	boolean isPrimaryKey() default false;

	/**
	 * 是否为数据库表中的字段 默认 true 是,false 不
	 */
	boolean exist() default true;
}
