package org.gongliang.common.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @description 注解实现日志记录
 * @author 龚亮
 * @date 2015-05-26 09:49:19
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(value = { ElementType.METHOD })
@Documented
public @interface KLog {
    /**
     * 业务的名称,例如:"修改菜单"
     */
    String value() default "";
}
