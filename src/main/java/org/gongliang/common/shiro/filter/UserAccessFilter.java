package org.gongliang.common.shiro.filter;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.shiro.web.filter.AccessControlFilter;
import org.gongliang.common.base.Result;

import com.alibaba.fastjson.JSON;


/**
 * 用户访问验证
 * 
 * @author 龚亮
 * @version 1.0
 */
public class UserAccessFilter extends AccessControlFilter {

	@Override
	protected boolean isAccessAllowed(ServletRequest request,
			ServletResponse response, Object mappedValue) throws Exception {

		return true;// true表示允许访问 false表示拒绝访问，进入onAccessDenied方法处理
	}

	@Override
	protected boolean onAccessDenied(ServletRequest request,
			ServletResponse response) throws Exception {
		Result result = new Result();
		response.setContentType("application/json");
		response.setCharacterEncoding("utf-8");
		response.getWriter().write(JSON.toJSONString(result));
		return false;
	}

}
