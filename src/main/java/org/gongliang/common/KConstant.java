package org.gongliang.common;

public class KConstant {

	/*
	 * 强制退出
	 */
	public static final String SESSION_FORCE_LOGOUT_KEY = "session.force.logout";
	/*
	 * token
	 */
	public static final String TOKEN_KEY = "x-auth-token";

}
