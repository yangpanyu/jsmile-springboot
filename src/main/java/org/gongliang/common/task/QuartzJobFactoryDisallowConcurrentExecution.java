package org.gongliang.common.task;

import org.gongliang.entity.sys.Task;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * 有状态任务
 * 
 * @description 若一个方法一次执行不完下次轮转时则等待改方法执行完后才执行下一次操作
 * @author 龚亮
 * @date 2015-05-27 14:22:13
 */
@DisallowConcurrentExecution
public class QuartzJobFactoryDisallowConcurrentExecution implements Job {

	public void execute(JobExecutionContext context) throws JobExecutionException {
		Task scheduleJob = (Task) context.getMergedJobDataMap().get("scheduleJob");
		TaskKit.invokMethod(scheduleJob);
	}

}