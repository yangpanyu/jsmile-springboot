package org.gongliang.common.task;

import org.gongliang.entity.sys.Task;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * 无状态任务
 * 
 * @description 计划任务执行处 无状态
 * @author 龚亮
 * @date 2015-05-27 14:22:13
 */
public class QuartzJobFactory implements Job {

	public void execute(JobExecutionContext context) throws JobExecutionException {
		Task scheduleJob = (Task) context.getMergedJobDataMap().get("scheduleJob");
		TaskKit.invokMethod(scheduleJob);
	}
}