package org.gongliang.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceBuilder;


@Configuration
@EnableTransactionManagement
@MapperScan("org.gongliang.dao")
public class MyBatisConfig{
	

    /**
     * druid数据库连接池
     */
    @ConfigurationProperties(prefix = "spring.datasource")
    @Bean(name="datasource",initMethod = "init",destroyMethod = "close")
    public DruidDataSource dataSource() {
		return DruidDataSourceBuilder.create().build();
    }
    
    /*
         开启注解事务
    @Bean
    public PlatformTransactionManager transactionManager(){
        return new DataSourceTransactionManager(dataSource());
    }*/

}
