package org.gongliang.kit;

import java.util.Map;

import com.google.common.collect.Maps;

import jodd.http.HttpRequest;
import jodd.http.HttpResponse;

/**
 * @description http工具类,未完成......
 * @author 龚亮
 * @datetime 2015-05-28 10:13:14
 */
public class HttpKit {
	
	public static void main(String[] args) throws Exception {
		Map<String,Object> param=Maps.newHashMap();
		post("http://localhost:8080/JSmile/sys_user/findAll", param);
	}
	/**
	 * http：get
	 * @param url： get请求地址
	 * @param param： get请求参数
	 * @return 请求返回的字符串(可以是json也可以是字符串)
	 */
	public static String get(String url,Map<String,Object> param){
		String result=null;
		try {
			 HttpRequest httpRequest = HttpRequest.get(url).form(param);
			 HttpResponse response = httpRequest.send();
			 result=response.bodyText();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	/**
	 * http：post
	 * @param url： post请求地址
	 * @param param： post请求参数
	 * @return 请求返回的字符串(可以是json也可以是字符串)
	 */
	public static String post(String url,Map<String,Object> param,Object... file){
		String result=null;
		try {
			HttpRequest httpRequest = HttpRequest.post(url)
					.header("x-auth-token", "00b5f3d2-0c4a-4846-80d7-c4eaf4bc18ba")
					                  .form(param);
			 HttpResponse response = httpRequest.send();
			 result=response.bodyText();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
}
