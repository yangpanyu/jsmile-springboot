package org.gongliang.kit.file;

import java.io.File;
import java.util.Date;
import java.util.UUID;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.gongliang.kit.str.StrKit;
import org.springframework.web.multipart.MultipartFile;


/**
 * 文件上传实体类
 * 
 * @author 龚亮
 * @version 1.0
 */
public class UploadFile {

	/**
	 * 随机生成的文件名称
	 */
	private String fileName;
	/**
	 * 随机生成的文件目录
	 */
	private String filePath;
	/**
	 * 原始文件名称
	 */
	private String originalFilename;
	private String contentType;
	private Long fileSize;
	/**
	 * oss文件上传后返回的md5,用于断点续传
	 */
	private String md5Name;

	/**
	 * prefix 文件上传的目录前缀(如)
	 */
	public UploadFile(MultipartFile file) {
		this(file, null);
	}

	public UploadFile(MultipartFile file, String prefix) {
		this.originalFilename = file.getOriginalFilename();
		this.fileName = generateRandonFileName(file.getOriginalFilename());
		this.contentType = getContentType(file.getOriginalFilename());
		this.fileSize = file.getSize();
		if (StrKit.isBlank(prefix)) {
			prefix = "";
		}
		filePath = prefix + generateRandomDateDir();
	}

	public UploadFile(File file) {
		this(file, null);
	}

	public UploadFile(File file, String prefix) {
		this.originalFilename = file.getName();
		this.contentType = getContentType(file.getName());
		this.fileSize = file.length();
		if (StrKit.isBlank(prefix)) {
			prefix = "";
		}
		this.filePath = "fs/upload/" + prefix + generateRandomDateDir();
		this.fileName = generateRandonFileName(file.getName());
	}
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getOriginalFilename() {
		return originalFilename;
	}

	public void setOriginalFilename(String originalFilename) {
		this.originalFilename = originalFilename;
	}

	public Long getFileSize() {
		return fileSize;
	}

	public void setFileSize(Long fileSize) {
		this.fileSize = fileSize;
	}
	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getMd5Name() {
		return md5Name;
	}

	public void setMd5Name(String md5Name) {
		this.md5Name = md5Name;
	}

	/**
	 * 获取文件路径
	 */
	public String getPath() {
		return this.filePath + this.fileName;
	}

	/**
	 * 获得日期生成二级目录
	 */
	public static String generateRandomDateDir() {
		Date date = new Date();
		return DateFormatUtils.format(date, "yyyy") + "/" + DateFormatUtils.format(date, "MM") + "/"
				+ DateFormatUtils.format(date, "dd") + "/";
	}

	public static String subFileName(String fileName) {
		int index = fileName.lastIndexOf(".");
		return -1 == index ? fileName : fileName.substring(index + 1);
	}

	/**
	 * 获取随机uuid文件名
	 */
	public static String generateRandonFileName(String filename) {
		return UUID.randomUUID().toString().replaceAll("-", "") + "." + subFileName(filename);
	}

	/**
	 * 通过文件名判断并获取OSS服务文件上传时文件的contentType
	 * 
	 * @param fileName
	 *            文件名
	 * @return 文件的contentType
	 */
	public static String getContentType(String fileName) {
		// 文件的后缀名
		String fileExtension = fileName.substring(fileName.lastIndexOf("."));
		if (".bmp".equalsIgnoreCase(fileExtension)) {
			return "image/bmp";
		}
		if (".gif".equalsIgnoreCase(fileExtension)) {
			return "image/gif";
		}
		if (".jpeg".equalsIgnoreCase(fileExtension) || ".jpg".equalsIgnoreCase(fileExtension)
				|| ".png".equalsIgnoreCase(fileExtension)) {
			return "image/jpeg";
		}
		if (".html".equalsIgnoreCase(fileExtension)) {
			return "text/html";
		}
		if (".txt".equalsIgnoreCase(fileExtension)) {
			return "text/plain";
		}
		if (".vsd".equalsIgnoreCase(fileExtension)) {
			return "application/vnd.visio";
		}
		if (".ppt".equalsIgnoreCase(fileExtension) || "pptx".equalsIgnoreCase(fileExtension)) {
			return "application/vnd.ms-powerpoint";
		}
		if (".doc".equalsIgnoreCase(fileExtension) || "docx".equalsIgnoreCase(fileExtension)) {
			return "application/msword";
		}
		if (".xml".equalsIgnoreCase(fileExtension)) {
			return "text/xml";
		}
		// 默认返回类型
		return "image/jpeg";
	}
}
