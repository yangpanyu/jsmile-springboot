package org.gongliang.kit;

import org.apache.shiro.crypto.hash.Md5Hash;

/**
 * 
 * @Description 加密工具类
 * @author 龚亮
 * @date 2017年3月25日 上午10:41:07
 */
public class HashKit {

	/**
	 * shiro加密工具类
	 * 
	 * @param value
	 * @param salt
	 * @param hashIterations
	 * @return Md5Hash
	 */
	public static Md5Hash md5(String value, String salt, int hashIterations) {
		return new Md5Hash(value, salt, hashIterations);
	}

	/**
	 * md5加密一次
	 */
	public static Md5Hash md5(String value) {
		return md5(value, "", 1);
	}

	/**
	 * md5加密hashIterations次
	 */
	public static Md5Hash md5(String value, int hashIterations) {
		return md5(value, "", hashIterations);
	}

	private static final char[] CHAR_ARRAY = "_-0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
			.toCharArray();
	private static final java.security.SecureRandom random = new java.security.SecureRandom();

	/**
	 * 生成随机salt
	 */
	public static String generateSalt(int saltLength) {
		StringBuilder salt = new StringBuilder();
		for (int i = 0; i < saltLength; i++) {
			salt.append(CHAR_ARRAY[random.nextInt(CHAR_ARRAY.length)]);
		}
		return salt.toString();
	}
}




