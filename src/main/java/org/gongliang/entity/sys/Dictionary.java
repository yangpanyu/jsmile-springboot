package org.gongliang.entity.sys;


/**
* 
* @author 龚亮
* @version 1.0
* @date 2017年06月19日 10:22:51
*/
public class Dictionary implements java.io.Serializable{

	private static final long serialVersionUID = 1L;
	 /**
	  * 注释: id
	  */
	  private Long id;
     
	 /**
	  * 注释: 标签
	  */
	  private String label;
     
	 /**
	  * 注释: 值
	  */
	  private String value;
     
	 /**
	  * 注释: parent_id
	  */
	  private Long parentId;
     
	 /**
	  * 注释: 类型
	  */
	  private String type;
     
	 /**
	  * 注释: 1正常  0删除 -1全部
	  */
	  private String status;
     
	 /**
	  * 注释: 描述
	  */
	  private String remark;
     

	public Long getId(){
        return id;
    }

    public void setId(Long id){
        this.id = id;
    }
	public String getLabel(){
        return label;
    }

    public void setLabel(String label){
        this.label = label;
    }
	public String getValue(){
        return value;
    }

    public void setValue(String value){
        this.value = value;
    }
	public Long getParentId(){
        return parentId;
    }

    public void setParentId(Long parentId){
        this.parentId = parentId;
    }
	public String getType(){
        return type;
    }

    public void setType(String type){
        this.type = type;
    }
	public String getStatus(){
        return status;
    }

    public void setStatus(String status){
        this.status = status;
    }
	public String getRemark(){
        return remark;
    }

    public void setRemark(String remark){
        this.remark = remark;
    }
}
