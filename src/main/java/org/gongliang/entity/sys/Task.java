package org.gongliang.entity.sys;


/**
* 
* @author 龚亮
* @version 1.0
* @date 2017年06月19日 10:22:51
*/
public class Task implements java.io.Serializable{

	private static final long serialVersionUID = 1L;

	public static final String STATUS_CEASE = "0";// 停止
	public static final String STATUS_NORMAL = "1";// 正常
	public static final String STATUS_RUNNING = "2";// 立即运行
	public static final String STATUS_STOP = "3";// 停止
	public static final String STATUS_RESUME = "4";// 恢复

	public static final String CONCURRENT_IS = "1";
	public static final String CONCURRENT_NOT = "0";

	 /**
	  * 注释: 编号
	  */
	  private Long id;
     
	 /**
	  * 注释: 任务名称
	  */
	  private String jobName;
     
	 /**
	  * 注释: 任务分组
	  */
	  private String jobGroup;
     
	 /**
	  * 注释: cron表达式
	  */
	  private String cron;
     
	 /**
	  * 注释: 任务执行时调用哪个类的方法 包名+类名
	  */
	  private String beanClass;
     
	 /**
	  * 注释: 是否有状态
	  */
	  private String isConcurrent;
     
	 /**
	  * 注释: 任务调用的方法名
	  */
	  private String methodName;
     
	 /**
	  * 注释: 创建者
	  */
	  private Long creator;
     
	 /**
	  * 注释: 创建时间
	  */
	  private java.util.Date createDate;
     
	 /**
	  * 注释: 修改者
	  */
	  private Long updater;
     
	 /**
	  * 注释: 修改时间
	  */
	  private java.util.Date updateDate;
     
	 /**
	  * 注释: 描述
	  */
	  private String remark;
     
	 /**
	  * 注释: 任务状态 1启用 0禁用  2立即执行 3暂停 4恢复任务
	  */
	  private String status;
     

	public Long getId(){
        return id;
    }

    public void setId(Long id){
        this.id = id;
    }
	public String getJobName(){
        return jobName;
    }

    public void setJobName(String jobName){
        this.jobName = jobName;
    }
	public String getJobGroup(){
        return jobGroup;
    }

    public void setJobGroup(String jobGroup){
        this.jobGroup = jobGroup;
    }
	public String getCron(){
        return cron;
    }

    public void setCron(String cron){
        this.cron = cron;
    }
	public String getBeanClass(){
        return beanClass;
    }

    public void setBeanClass(String beanClass){
        this.beanClass = beanClass;
    }
	public String getIsConcurrent(){
        return isConcurrent;
    }

    public void setIsConcurrent(String isConcurrent){
        this.isConcurrent = isConcurrent;
    }
	public String getMethodName(){
        return methodName;
    }

    public void setMethodName(String methodName){
        this.methodName = methodName;
    }
	public Long getCreator(){
        return creator;
    }

    public void setCreator(Long creator){
        this.creator = creator;
    }
	public java.util.Date getCreateDate(){
        return createDate;
    }

    public void setCreateDate(java.util.Date createDate){
        this.createDate = createDate;
    }
	public Long getUpdater(){
        return updater;
    }

    public void setUpdater(Long updater){
        this.updater = updater;
    }
	public java.util.Date getUpdateDate(){
        return updateDate;
    }

    public void setUpdateDate(java.util.Date updateDate){
        this.updateDate = updateDate;
    }
	public String getRemark(){
        return remark;
    }

    public void setRemark(String remark){
        this.remark = remark;
    }
	public String getStatus(){
        return status;
    }

    public void setStatus(String status){
        this.status = status;
    }
}
