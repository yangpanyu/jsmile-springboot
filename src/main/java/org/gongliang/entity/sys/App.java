package org.gongliang.entity.sys;


/**
* 
* @author 龚亮
* @version 1.0
* @date 2017年06月19日 10:22:51
*/
public class App implements java.io.Serializable{

	private static final long serialVersionUID = 1L;
	 /**
	  * 注释: id
	  */
	  private Long id;
     
	 /**
	  * 注释: 版本号
	  */
	  private String versionName;
     
	 /**
	  * 注释: 自然数版本号
	  */
	  private String versionCode;
     
	 /**
	  * 注释: app地址
	  */
	  private String appUrl;
     
	 /**
	  * 注释: 是否强制升级  1/true是  0/false否
	  */
	  private Boolean isImposed;
     
	 /**
	  * 注释: 1 安卓 2苹果 3其他 -1全部
	  */
	  private String type;
     
	 /**
	  * 注释: 描述
	  */
	  private String remark;
     
	 /**
	  * 注释: 1正常  0删除 -1全部
	  */
	  private String status;
     
	 /**
	  * 注释: 创建者
	  */
	  private Long creator;
     
	 /**
	  * 注释: 更新者
	  */
	  private Long updater;
     
	 /**
	  * 注释: 创建时间
	  */
	  private java.util.Date createDate;
     
	 /**
	  * 注释: 更新时间
	  */
	  private java.util.Date updateDate;
     

	public Long getId(){
        return id;
    }

    public void setId(Long id){
        this.id = id;
    }
	public String getVersionName(){
        return versionName;
    }

    public void setVersionName(String versionName){
        this.versionName = versionName;
    }
	public String getVersionCode(){
        return versionCode;
    }

    public void setVersionCode(String versionCode){
        this.versionCode = versionCode;
    }
	public String getAppUrl(){
        return appUrl;
    }

    public void setAppUrl(String appUrl){
        this.appUrl = appUrl;
    }
	public Boolean getIsImposed(){
        return isImposed;
    }

    public void setIsImposed(Boolean isImposed){
        this.isImposed = isImposed;
    }
	public String getType(){
        return type;
    }

    public void setType(String type){
        this.type = type;
    }
	public String getRemark(){
        return remark;
    }

    public void setRemark(String remark){
        this.remark = remark;
    }
	public String getStatus(){
        return status;
    }

    public void setStatus(String status){
        this.status = status;
    }
	public Long getCreator(){
        return creator;
    }

    public void setCreator(Long creator){
        this.creator = creator;
    }
	public Long getUpdater(){
        return updater;
    }

    public void setUpdater(Long updater){
        this.updater = updater;
    }
	public java.util.Date getCreateDate(){
        return createDate;
    }

    public void setCreateDate(java.util.Date createDate){
        this.createDate = createDate;
    }
	public java.util.Date getUpdateDate(){
        return updateDate;
    }

    public void setUpdateDate(java.util.Date updateDate){
        this.updateDate = updateDate;
    }
}
