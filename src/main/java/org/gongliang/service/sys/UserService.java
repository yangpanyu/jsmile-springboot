package org.gongliang.service.sys;

import org.gongliang.common.base.CRUDService;
import org.gongliang.dao.sys.UserDao;
import org.gongliang.entity.sys.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
* 
* @author 龚亮
* @version 1.0
* @date 2017年05月04日 01:40:25
*/
@Service
public class UserService extends CRUDService<UserDao, User> {

	@Autowired
	private UserDao userDao;

	/**
	 * 用户密码修改
	 * @param userId 用户id
	 * @param password 原始密码
	 * @param newPassword 新密码
	 * @return
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public boolean updatePassword(Long userId, String password, String newPassword) {
		return retBool(userDao.updatePassword(userId, password, newPassword));
	}

	/**
	 * 根据用户名查找用户
	 */
	@Transactional(readOnly = true, rollbackFor = Exception.class)
	public User findUser(String username) {
		return userDao.findUser(username);
	}

}
