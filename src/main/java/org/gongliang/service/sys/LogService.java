package org.gongliang.service.sys;

import java.util.List;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.gongliang.common.base.CRUDService;
import org.gongliang.dao.sys.LogDao;
import org.gongliang.entity.sys.Log;
import org.gongliang.kit.web.SpringKit;
import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
* 
* @author 龚亮
* @version 1.0
* @date 2017年05月04日 01:40:25
*/
@Service
public class LogService extends CRUDService<LogDao, Log> {

	public static final Logger logger = LoggerFactory.getLogger(LogService.class);

	public boolean saveBatch(List<Log> logs) {
		SqlSessionTemplate sqlSessionTemplate = SpringKit.getBean(SqlSessionTemplate.class);
		SqlSession sqlSession = null;
		int result = 1;
		try {
			sqlSession = sqlSessionTemplate.getSqlSessionFactory().openSession(ExecutorType.BATCH, false);
			int batchCount = 1000;
			int lastBatchCount = batchCount;
			if (logs != null && logs.size() > 0) {
				for (int index = 0; index < logs.size(); index++) {
					if (lastBatchCount >= logs.size()) {
						lastBatchCount = logs.size();
						result = result * sqlSession.insert("org.gongliang.dao.sys.LogDao.batchSave",
								logs.subList(index, lastBatchCount));
						sqlSession.commit();
						break;
					} else {
						result = result * sqlSession.insert("org.gongliang.dao.sys.LogDao.batchSave",
								logs.subList(index, lastBatchCount));
						sqlSession.commit();
						index = lastBatchCount;
						lastBatchCount = index + (batchCount - 1);
					}
				}
				sqlSession.commit();
			}
		} catch (Exception e) {
			logger.error("批量插入日志失败:" + ExceptionUtils.getStackTrace(e));
		} finally {
			sqlSession.close();
		}
		return retBool(result);
	}

}
