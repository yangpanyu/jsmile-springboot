package org.gongliang.service.sys;

import java.util.Date;
import java.util.List;

import org.gongliang.common.base.CRUDService;
import org.gongliang.dao.sys.ResourcesDao;
import org.gongliang.entity.sys.Resources;
import org.gongliang.kit.web.ShiroKit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
* 
* @author 龚亮
* @version 1.0
* @date 2017年05月04日 01:40:25
*/
@Service
public class ResourcesService extends CRUDService<ResourcesDao, Resources> {

	@Autowired
	private ResourcesDao resourcesDao;

	/**
	 * 
	 * @Description 查询用户所拥有的权限
	 * @author 龚亮
	 * @param userId
	 *            用户id
	 * @param type
	 *            权限类型 1菜单 2权限 -1全部
	 * @return 权限集合
	 */
	@Transactional(readOnly = true, rollbackFor = Exception.class)
	public List<Resources> getUserPermission(Long userId, String type) {
		return resourcesDao.getUserPermission(userId, type);
	}

	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public boolean saveBasePermission(Long parentId, Integer level) {
		Resources save = new Resources();
		save.setName("添加");
		save.setCreateDate(new Date());
		save.setCreator(ShiroKit.getLoginUser().getId());
		save.setIconCls("sn-add");
		save.setParentId(parentId);
		save.setLevel(level);
		save.setSort(101);
		save.setPermCode("create");
		save.setType(Resources.RESOURCES_PERMISSION);

		Resources delete = new Resources();
		delete.setName("删除");
		delete.setCreateDate(new Date());
		delete.setCreator(ShiroKit.getLoginUser().getId());
		delete.setIconCls("sn-remove");
		delete.setParentId(parentId);
		delete.setLevel(level);
		delete.setSort(102);
		delete.setPermCode("delete");
		delete.setType(Resources.RESOURCES_PERMISSION);

		Resources update = new Resources();
		update.setName("修改");
		update.setCreateDate(new Date());
		update.setCreator(ShiroKit.getLoginUser().getId());
		update.setIconCls("sn-edit");
		update.setParentId(parentId);
		update.setLevel(level);
		update.setSort(103);
		update.setPermCode("update");
		update.setType(Resources.RESOURCES_PERMISSION);

		Resources query = new Resources();
		query.setName("查询");
		query.setCreateDate(new Date());
		query.setCreator(ShiroKit.getLoginUser().getId());
		query.setIconCls("sn-search");
		query.setParentId(parentId);
		query.setLevel(level);
		query.setSort(101);
		query.setPermCode("list");
		query.setType(Resources.RESOURCES_PERMISSION);

		int i = 0;
		i += resourcesDao.save(save);
		i += resourcesDao.save(update);
		i += resourcesDao.save(query);
		i += resourcesDao.save(delete);
		return retBool(i);
	}
}
