package org.gongliang.service.sys;

import org.gongliang.common.base.CRUDService;
import org.gongliang.dao.sys.DictionaryDao;
import org.gongliang.entity.sys.Dictionary;
import org.springframework.stereotype.Service;

/**
* 
* @author 龚亮
* @version 1.0
* @date 2017年05月04日 01:40:24
*/
@Service
public class DictionaryService extends CRUDService<DictionaryDao, Dictionary> {

}
