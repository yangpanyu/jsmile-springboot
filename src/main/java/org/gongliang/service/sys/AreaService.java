package org.gongliang.service.sys;

import org.gongliang.common.base.CRUDService;
import org.gongliang.dao.sys.AreaDao;
import org.gongliang.entity.sys.Area;
import org.springframework.stereotype.Service;

/**
* 
* @author 龚亮
* @version 1.0
* @date 2017年05月04日 01:40:24
*/
@Service
public class AreaService extends CRUDService<AreaDao, Area> {

}
