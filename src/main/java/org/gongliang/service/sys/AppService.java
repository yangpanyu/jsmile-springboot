package org.gongliang.service.sys;

import org.gongliang.entity.sys.App;
import org.gongliang.common.base.CRUDService;
import org.gongliang.dao.sys.AppDao;
import org.springframework.stereotype.Service;

/**
* 
* @author 龚亮
* @version 1.0
* @date 2017年05月04日 01:40:24
*/
@Service
public class AppService extends CRUDService<AppDao, App> {

}
