package org.gongliang.controller.sys;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.gongliang.common.annotation.KLog;
import org.gongliang.common.base.BaseController;
import org.gongliang.common.base.Result;
import org.gongliang.common.exception.ExceptionEnum;
import org.gongliang.entity.sys.Task;
import org.gongliang.kit.str.StrKit;
import org.gongliang.kit.web.WebKit;
import org.gongliang.service.sys.TaskService;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageInfo;

/**
* @desc   控制器
* @author 龚亮
* @version 1.0
* @date 2017年05月15日 02:52:17
*/
@Controller
@RequestMapping("/sys_task")
public class TaskController extends BaseController {

    @Autowired
	private TaskService taskService;

	@RequiresPermissions("sys:task:view")
	@GetMapping("/main")
	public String main() {
		return "sys/Task/main";
	}

	@GetMapping("/updatePage")
	public String updatePage() {
		return "sys/Task/task";
	}

	@ResponseBody
	@RequestMapping(value = "/findAllByPage", method = { RequestMethod.GET, RequestMethod.POST })
	public Result findAllByPage(HttpServletRequest request) {
		Map<String, Object> reqMap = WebKit.getParams(request);
		WebKit.setPage(WebKit.toPageIndex(reqMap), WebKit.toPageSize(reqMap));
		List<Task> list = taskService.findAll(reqMap);
		PageInfo<Task> pages = new PageInfo<Task>(list);
		Result result = Result.success();
		result.setResData(pages.getList(), pages.getTotal());
		return result;
	}

	/**
	 * 添加或者修改 主键id不为空时更新,否则保存
	 */
	@KLog("任务调度->添加任务或修改任务")
	@RequiresPermissions({ "sys:task:create", "sys:task:update" })
	@ResponseBody
	@RequestMapping(value = "/saveORupdate", method = RequestMethod.POST)
	public Result saveORupdate(Task task) {
		if (task == null) {
			return Result.error(ExceptionEnum.PARAM_NOT_NULL);
		}
		if (task.getId() == null) {
			task.setCreateDate(new Date());
			task.setCreator(getUserId());
			return taskService.save(task) ? Result.success("保存信息成功!") : Result.error("保存信息失败!");
		} else {
			task.setUpdateDate(new Date());
			task.setUpdater(getUserId());
			return taskService.updateById(task) ? Result.success("更新信息成功!") : Result.error("更新信息失败!");
		}
	}

	/**
	 * 删除任务
	 */
	@KLog("任务调度->删除任务")
	@RequiresPermissions("sys:task:delete")
	@ResponseBody
	@RequestMapping(value = "/deleteById", method = RequestMethod.POST)
	public Result deleteByIds(String id) throws SchedulerException {
		if (StrKit.isBlank(id)) {
			return Result.error(ExceptionEnum.ID_NOT_NULL);
		}
		Task task = taskService.findById(id);
		taskService.deleteJob(task);
		return taskService.deleteById(id) ? Result.success("删除任务成功!") : Result.error("删除任务失败!");
	}
	/*
	 * 更改任务状态
	 */
	@ResponseBody
	@RequestMapping(value = "/changeJobStatus", method = RequestMethod.POST)
	public Result changeJobStatus(Task task) throws SchedulerException {
		if (task == null) {
			return Result.error("请求参数不能为空!");
		}
		return taskService.changeJobStatus(task) ? Result.success("任务状态修改成功!") : Result.error("任务状态修改失败!");
	}

}
