package org.gongliang.controller.sys;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.gongliang.common.base.BaseController;
import org.gongliang.common.base.Result;
import org.gongliang.common.exception.ExceptionEnum;
import org.gongliang.entity.sys.Log;
import org.gongliang.kit.web.WebKit;
import org.gongliang.service.sys.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageInfo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
* @desc   控制器
* @author 龚亮
* @version 1.0
* @date 2017年05月15日 02:52:17
*/
@Controller
@RequestMapping("/sys_log")
@Api("日志管理")
public class LogController extends BaseController {

    @Autowired
    private LogService logService;

	@RequiresPermissions("sys:log:view")
	@GetMapping("/main")
	public String main() {
		return "sys/Log/main";
	}

	/**
	 * 查询所有分页 返回 Lit<Map>
	 */
	@ResponseBody
	@RequestMapping(value = "/findAllByPage", method = { RequestMethod.GET, RequestMethod.POST })
	@ApiOperation(value = "日志记录接口")
	public Result findAllMapByPage(HttpServletRequest request) {
		Map<String, Object> reqMap = WebKit.getParams(request);
		WebKit.setPage(WebKit.toPageIndex(reqMap), WebKit.toPageSize(reqMap));
		List<Log> list = logService.findAll(reqMap);
		PageInfo<Log> pages = new PageInfo<Log>(list);
		Result result = Result.success();
		result.setResData(pages.getList(), pages.getTotal());
		return result;
	}

	/**
	 * 根据一组id删除
	 */
	@RequiresPermissions("sys:log:delete")
	@ResponseBody
	@RequestMapping(value = "/deleteByIds", method = RequestMethod.POST)
	public Result deleteByIds(@RequestParam(value = "ids[]", required = true) String[] ids) {
		if (ids == null || ids.length == 0) {
			return Result.error(ExceptionEnum.IDS_NOT_NULL);
		}
		return logService.deleteByIds(ids) ? Result.success("批量删除角色成功!") : Result.error("批量删除用户失败!");
	}
}
