package org.gongliang.controller.sys;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.gongliang.common.annotation.KLog;
import org.gongliang.common.base.BaseController;
import org.gongliang.common.base.Result;
import org.gongliang.common.exception.ExceptionEnum;
import org.gongliang.entity.sys.Resources;
import org.gongliang.kit.web.WebKit;
import org.gongliang.service.sys.ResourcesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
* @desc   控制器
* @author 龚亮
* @version 1.0
* @date 2017年05月15日 02:52:17
*/
@Controller
@RequestMapping("/sys_resources")
public class ResourcesController extends BaseController {

    @Autowired
    private ResourcesService resourcesService;

	@RequiresPermissions("sys:resources:view")
	@GetMapping("/main")
	public String main() {
		return "sys/Resources/main";
	}

	@GetMapping("/updatePage")
	public String updatePage() {
		return "sys/Resources/res";
	}

	@ResponseBody
	@RequestMapping(value = "/findAll", method = { RequestMethod.GET, RequestMethod.POST })
	public Result findAll(HttpServletRequest request) {
		Map<String, Object> reqMap = WebKit.getParams(request);
		List<Resources> list = resourcesService.findAll(reqMap);
		Result result = Result.success();
		result.setResData(list, list.size() + 0L);
		return result;
	}

	/**
	 * 添加或者修改 主键id不为空时更新,否则保存
	 */
	@KLog("资源管理->添加或修改资源")
	@RequiresPermissions({ "sys:resources:create", "sys:resources:update" })
	@ResponseBody
	@RequestMapping(value = "/saveORupdate", method = RequestMethod.POST)
	public Result saveORupdate(Resources resources) {
		if (resources == null) {
			return Result.error(ExceptionEnum.PARAM_NOT_NULL);
		}
		if (resources.getId() == null) {
			resources.setCreateDate(new Date());
			resources.setCreator(getUserId());
			return resourcesService.save(resources) ? Result.success("保存资源信息成功!") : Result.error("保存资源信息失败!");
		} else {
			resources.setUpdateDate(new Date());
			resources.setUpdater(getUserId());
			return resourcesService.updateById(resources) ? Result.success("更新资源信息成功!") : Result.error("更新资源信息失败!");
		}
	}

	/**
	 * 根据一组id删除
	 */
	@KLog("字典管理->删除字典")
	@RequiresPermissions("sys:resources:delete")
	@ResponseBody
	@RequestMapping(value = "/deleteByIds", method = RequestMethod.POST)
	public Result deleteByIds(@RequestParam(value = "ids[]", required = true) String[] ids) {
		if (ids == null || ids.length == 0) {
			return Result.error(ExceptionEnum.IDS_NOT_NULL);
		}
		return resourcesService.deleteByIds(ids) ? Result.success("删除资源成功!") : Result.error("删除资源失败!");
	}

	@RequiresPermissions("sys:resources:saveBasePermission")
	@ResponseBody
	@RequestMapping(value = "/saveBasePermission", method = { RequestMethod.POST })
	public Result saveBasePermission(@RequestParam(value = "parentId", required = true) Long parentId, Integer level) {
		return resourcesService.saveBasePermission(parentId, level) ? Result.success("添加基本权限成功!")
				: Result.error("添加基本权限失败!");
	}
}
