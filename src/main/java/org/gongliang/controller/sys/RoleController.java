package org.gongliang.controller.sys;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.gongliang.common.annotation.KLog;
import org.gongliang.common.base.BaseController;
import org.gongliang.common.base.Result;
import org.gongliang.common.exception.ExceptionEnum;
import org.gongliang.entity.sys.Resources;
import org.gongliang.entity.sys.Role;
import org.gongliang.kit.web.WebKit;
import org.gongliang.service.sys.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageInfo;

/**
* @desc   控制器
* @author 龚亮
* @version 1.0
* @date 2017年05月15日 02:52:17
*/
@Controller
@RequestMapping("/sys_role")
public class RoleController extends BaseController {

    @Autowired
    private RoleService roleService;

	@RequiresPermissions("sys:role:view")
	@GetMapping("/main")
	public String main() {
		return "sys/Role/main";
	}

	@GetMapping("/resPage")
	public String resPage(Model model, String roleId) {
		model.addAttribute("roleId", roleId);
		return "sys/Role/res";
	}

	@GetMapping("/updatePage")
	public String updatePage() {
		return "sys/Role/role";
	}

	/**
	 * 查询所有分页 返回 Lit<Map>
	 */
	@ResponseBody
	@RequestMapping(value = "/findAllByPage", method = { RequestMethod.GET, RequestMethod.POST })
	public Result findAllByPage(HttpServletRequest request) {
		Map<String, Object> reqMap = WebKit.getParams(request);
		WebKit.setPage(WebKit.toPageIndex(reqMap), WebKit.toPageSize(reqMap));
		List<Role> list = roleService.findAll(reqMap);
		PageInfo<Role> pages = new PageInfo<Role>(list);
		Result result = Result.success();
		result.setResData(pages.getList(), pages.getTotal());
		return result;
	}

	@ResponseBody
	@RequestMapping(value = "/findAll", method = { RequestMethod.GET, RequestMethod.POST })
	public Result findAll(HttpServletRequest request) {
		Map<String, Object> reqMap = WebKit.getParams(request);
		List<Role> list = roleService.findAll(reqMap);
		Result result = Result.success();
		result.setResData(list, list.size() + 0L);
		return result;
	}

	/**
	 * 添加或者修改 主键id不为空时更新,否则保存
	 */
	@ResponseBody
	@RequestMapping(value = "/saveORupdate", method = RequestMethod.POST)
	@RequiresPermissions({ "sys:role:create", "sys:role:update" })
	@KLog("角色管理->添加或修改角色")
	public Result saveORupdate(Role role) {
		if (role == null) {
			return Result.error(ExceptionEnum.PARAM_NOT_NULL);
		}
		if (role.getId() == null) {
			role.setCreateDate(new Date());
			role.setCreator(getUserId());
			return roleService.save(role) ? Result.success("保存角色信息成功!") : Result.error("保存用户信息失败!");
		} else {
			role.setUpdateDate(new Date());
			role.setUpdater(getUserId());
			return roleService.updateById(role) ? Result.success("更新角色信息成功!") : Result.error("更新用户信息失败!");
		}
	}

	/**
	 * 根据一组id删除
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteByIds", method = RequestMethod.POST)
	@RequiresPermissions("sys:role:delete")
	@KLog("角色管理->删除角色")
	public Result deleteByIds(@RequestParam(value = "ids[]", required = true) String[] ids) {
		if (ids == null || ids.length == 0) {
			return Result.error(ExceptionEnum.IDS_NOT_NULL);
		}
		return roleService.deleteByIds(ids) ? Result.success("批量删除角色成功!") : Result.error("批量删除用户失败!");
	}

	/*
	 * 获取用户角色
	 */
	@ResponseBody
	@RequestMapping(value = "/getUserRole", method = { RequestMethod.GET, RequestMethod.POST })
	public Result getUserRole(@RequestParam(value = "userId", required = true) Long userId) {
		Result result = Result.success("请求成功!");
		List<Role> list = roleService.getUserRole(userId);
		result.setResData(list);
		return result;
	}

	/*
	 * 保存用户角色
	 */
	@ResponseBody
	@RequestMapping(value = "/saveUserRole", method = { RequestMethod.POST })
	@KLog("用户管理->保存用户角色添加或修改角色")
	@RequiresPermissions({ "sys:user:saveUserRole" })
	public Result saveUserRole(@RequestParam(value = "userId", required = true) Long userId,
			@RequestParam(value = "roleIds[]", required = false) List<Long> roleIds) {
		if (roleService.saveUserRole(userId, roleIds)) {
			return Result.success("保存用户角色成功!");
		}
		return Result.error("保存用户角色失败!");
	}

	/*
	 * 根据角色id获取角色资源信息
	 */
	@ResponseBody
	@RequestMapping(value = "/getRoleResources", method = { RequestMethod.GET, RequestMethod.POST })
	public Result getRoleResource(@RequestParam(value = "roleId", required = true) Long roleId) {
		Result result = Result.success("请求成功!");
		List<Resources> list = roleService.getRoleResource(roleId);
		result.setResData(list);
		return result;
	}

	/*
	 * 保存角色资源
	 */
	@ResponseBody
	@RequestMapping(value = "/saveRoleResources", method = { RequestMethod.POST })
	@RequiresPermissions({ "sys:role:saveRoleResource" })
	@KLog("角色管理->保存角色权限")
	public Result saveRoleResource(@RequestParam(value = "roleId", required = true) Long roleId,
			@RequestParam(value = "resourcesIds[]", required = false) List<Long> resourceIds) {
		if (roleService.saveRoleResources(roleId, resourceIds)) {
			return Result.success("保存角色资源成功!");
		}
		return Result.error("保存角色资源失败!");
	}
}
