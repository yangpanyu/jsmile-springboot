package org.gongliang.controller.sys;

import java.io.File;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.gongliang.common.KConfig;
import org.gongliang.kit.file.FileKit;
import org.gongliang.kit.file.UploadFile;
import org.gongliang.kit.str.StrKit;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baidu.ueditor.ActionEnter;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import springfox.documentation.annotations.ApiIgnore;

@Controller
@ApiIgnore
public class UeditorController {

    
	@RequestMapping("/ueditor/config")
	@ResponseBody
	public String ueditor(HttpServletRequest request) {
		String rootPath = request.getSession().getServletContext().getRealPath("/");
		String result = new ActionEnter(request, rootPath).exec();
		return result;
	}

	@RequestMapping("/ueditor/uploadImage")
	@ResponseBody
	public Map<String, Object> uploadImage(HttpServletRequest request) {
		return upload("ueditor/image/", request);
	}

	@RequestMapping("/ueditor/uploadFile")
	@ResponseBody
	public Map<String, Object> uploadFile(HttpServletRequest request) {
		return upload("ueditor/file/", request);
	}

	@RequestMapping("/ueditor/uploadVideo")
	@ResponseBody
	public Map<String, Object> uploadVideo(HttpServletRequest request) {
		return upload("ueditor/video/", request);
	}

	@RequestMapping("/ueditor/listFile")
	@ResponseBody
	public Map<String, Object> listFile(HttpServletRequest request) {
		return uploadList(KConfig.me().getFileUploadPath() + "ueditor/file/",
				request);
	}

	@RequestMapping("/ueditor/listImage")
	@ResponseBody
	public Map<String, Object> listImage(HttpServletRequest request) {
		return uploadList(
				KConfig.me().getFileUploadPath() + "ueditor/image/",
				request);
	}

	private Map<String, Object> uploadList(String filename, HttpServletRequest request) {
		Map<String, Object> m = Maps.newHashMap();
		m.put("state", "SUCCESS");
		m.put("total", request.getParameter("size"));
		m.put("start", request.getParameter("start"));
		File file = new File(filename);
		if (!file.exists() && !file.isDirectory())
			file.mkdirs();
		Collection<File> list = FileUtils.listFiles(file, FileFilterUtils.suffixFileFilter(""), DirectoryFileFilter.INSTANCE);
		List<Map<String, Object>> urls = Lists.newArrayList();
		for (File f : list) {
			String path = f.getAbsolutePath();
			if (StrKit.isWindows()) {
				path = path.replaceAll("\\\\", "/");
			}
			Map<String, Object> url = Maps.newHashMap();
			url.put("state", "SUCCESS");
			path = path.replaceAll(KConfig.me().getFileUploadPath(), KConfig.me().getFileDownPath());
			url.put("url", path);
			urls.add(url);
		}
		m.put("list", urls);
		return m;
	}

	private Map<String, Object> upload(String filename, HttpServletRequest request) {
		Map<String, Object> m = Maps.newHashMap();
		UploadFile file = FileKit.upload(request, filename);
		m.put("state", "SUCCESS");
		m.put("url", KConfig.me().getFileDownPath() + file.getPath());
		m.put("title", file.getOriginalFilename());
		m.put("original", request.getParameter("name"));
		m.put("type", request.getParameter("type"));
		m.put("size", request.getParameter("size"));
		return m;
	}
}
