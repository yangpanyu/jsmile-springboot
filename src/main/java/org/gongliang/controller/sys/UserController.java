package org.gongliang.controller.sys;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.gongliang.common.annotation.KLog;
import org.gongliang.common.base.BaseController;
import org.gongliang.common.base.Result;
import org.gongliang.common.exception.ExceptionEnum;
import org.gongliang.entity.sys.User;
import org.gongliang.kit.web.WebKit;
import org.gongliang.service.sys.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageInfo;

/**
* @desc   控制器
* @author 龚亮
* @version 1.0
* @date 2017年05月15日 02:52:17
*/
@Controller
@RequestMapping("/sys_user")
public class UserController extends BaseController {

    @Autowired
    private UserService userService;

	/**
	 * 跳转到用户页
	 */
	@RequiresPermissions("sys:user:view")
	@GetMapping("/main")
	public String main() {
		return "sys/User/main";
	}

	@GetMapping("/rolePage")
	public String rolePage(Model model, String userId) {
		model.addAttribute("userId", userId);
		return "sys/User/role";
	}
	@GetMapping("/updatePage")
	public String updatePage() {
		return "sys/User/user";
	}
	/**
	 * 修改密码
	 */
	@KLog("用户信息->修改密码")
	@ResponseBody
	@RequestMapping(value = "/updatePassword", method = RequestMethod.POST)
	public Result updatePassword(String password, String newPassword) {
		password = new Md5Hash(password, "", 2).toString();
		newPassword = new Md5Hash(newPassword, "", 2).toString();
		if (userService.updatePassword(getUser().getId(), password, newPassword)) {
			return Result.success("密码修改成功!");
		}
		return Result.error("原密码不正确!");
	}

	/**
	 * 查询所有分页 返回 Lit<Map>
	 */
	@ResponseBody
	@RequestMapping(value = "/findAllMapByPage", method = { RequestMethod.GET, RequestMethod.POST })
	public Result findAllMapByPage(HttpServletRequest request) {
		Map<String, Object> reqMap = WebKit.getParams(request);
		WebKit.setPage(WebKit.toPageIndex(reqMap), WebKit.toPageSize(reqMap));
		List<Map<String, Object>> list = userService.findAllMap(reqMap);
		PageInfo<Map<String, Object>> pages = new PageInfo<Map<String, Object>>(list);
		Result result = Result.success();
		result.setResData(pages.getList(), pages.getTotal());
		return result;
	}

	/**
	 * 添加或者修改 主键id不为空时更新,否则保存
	 */
	@KLog("用户信息->添加或修改用户")
	@RequiresPermissions({ "sys:user:create", "sys:user:update" })
	@ResponseBody
	@RequestMapping(value = "/saveORupdate", method = RequestMethod.POST)
	public Result saveORupdate(User user) {
		if (user == null) {
			return Result.error(ExceptionEnum.PARAM_NOT_NULL);
		}
		if (user.getId() == null) {
			String password = new Md5Hash("123456", "", 2).toString();
			user.setPassword(password);
			user.setCreateDate(new Date());
			user.setCreator(getUserId());
			return userService.save(user) ? Result.success("保存用户信息成功!") : Result.error("保存用户信息失败!");
		} else {
			user.setUpdateDate(new Date());
			user.setUpdater(getUserId());
			return userService.updateById(user) ? Result.success("更新用户信息成功!") : Result.error("更新用户信息失败!");
		}
	}

	/**
	 * 根据一组id删除
	 */
	@KLog("用户信息->批量删除用户")
	@RequiresPermissions("sys:user:delete")
	@ResponseBody
	@RequestMapping(value = "/deleteByIds", method = RequestMethod.POST)
	public Result deleteByIds(@RequestParam(value = "ids[]", required = true) String[] ids) {
		if (ids == null || ids.length == 0) {
			return Result.error(ExceptionEnum.IDS_NOT_NULL);
		}
		if (ArrayUtils.contains(ids, getUser().getId().toString())) {
			return Result.error("当前用户不能删除!");
		}
		if (ArrayUtils.contains(ids, "1")) {
			return Result.error("管理员不能删除!");
		}
		return userService.deleteByIds(ids) ? Result.success("批量删除用户成功!") : Result.error("批量删除用户失败!");
	}

}
