package org.gongliang.controller.sys;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.gongliang.common.annotation.KLog;
import org.gongliang.common.base.BaseController;
import org.gongliang.common.base.Result;
import org.gongliang.common.exception.ExceptionEnum;
import org.gongliang.entity.sys.Department;
import org.gongliang.kit.web.WebKit;
import org.gongliang.service.sys.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
* @desc   控制器
* @author 龚亮
* @version 1.0
* @date 2017年05月15日 02:52:17
*/
@Controller
@RequestMapping("/sys_dept")
public class DepartmentController extends BaseController {

    @Autowired
    private DepartmentService departmentService;

	@RequiresPermissions("sys:dept:view")
	@GetMapping("/main")
	public String main() {
		return "sys/Department/main";
	}

	@GetMapping("/updatePage")
	public String updatePage() {
		return "sys/Department/dept";
	}

	@ResponseBody
	@RequestMapping(value = "/findAll", method = { RequestMethod.GET, RequestMethod.POST })
	public Result findAll(HttpServletRequest request) {
		Map<String, Object> reqMap = WebKit.getParams(request);
		List<Department> list = departmentService.findAll(reqMap);
		Result result = Result.success();
		result.setResData(list, list.size() + 0L);
		return result;
	}
	/**
	 * 添加或者修改 主键id不为空时更新,否则保存
	 */
	@KLog("部门管理->添加或修改部门信息")
	@RequiresPermissions({ "sys:dept:create", "sys:dept:update" })
	@ResponseBody
	@RequestMapping(value = "/saveORupdate", method = RequestMethod.POST)
	public Result saveORupdate(Department dept) {
		if (dept == null) {
			return Result.error(ExceptionEnum.PARAM_NOT_NULL);
		}
		if (dept.getId() == null) {
			dept.setCreateDate(new Date());
			dept.setCreator(getUserId());
			return departmentService.save(dept) ? Result.success("保存部门信息成功!") : Result.error("保存部门信息失败!");
		} else {
			dept.setUpdateDate(new Date());
			dept.setUpdater(getUserId());
			return departmentService.updateById(dept) ? Result.success("更新部门信息成功!") : Result.error("更新部门信息失败!");
		}
	}

	/**
	 * 根据一组id删除
	 */
	@KLog("部门管理->删除部门")
	@RequiresPermissions("sys:dept:delete")
	@ResponseBody
	@RequestMapping(value = "/deleteByIds", method = RequestMethod.POST)
	public Result deleteByIds(@RequestParam(value = "ids[]", required = true) String[] ids) {
		if (ids == null || ids.length == 0) {
			return Result.error(ExceptionEnum.IDS_NOT_NULL);
		}
		return departmentService.deleteByIds(ids) ? Result.success("删除部门成功!") : Result.error("删除部门失败!");
	}
}
