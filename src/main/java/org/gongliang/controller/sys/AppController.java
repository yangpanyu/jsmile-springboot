package org.gongliang.controller.sys;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.gongliang.common.annotation.KLog;
import org.gongliang.common.base.BaseController;
import org.gongliang.common.base.Result;
import org.gongliang.common.exception.ExceptionEnum;
import org.gongliang.entity.sys.App;
import org.gongliang.kit.web.WebKit;
import org.gongliang.service.sys.AppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageInfo;

/**
* @desc   控制器
* @author 龚亮
* @version 1.0
* @date 2017年05月15日 02:52:16
*/
@Controller
@RequestMapping("/sys_app")
public class AppController extends BaseController {

    @Autowired
    private AppService appService;

	@RequiresPermissions("sys:app:view")
	@GetMapping("/main")
	public String main() {
		return "sys/App/main";
	}

	@GetMapping("/updatePage")
	public String updatePage() {
		return "sys/App/app";
	}
	@ResponseBody
	@RequestMapping(value = "/findAllByPage", method = { RequestMethod.GET, RequestMethod.POST })
	public Result findAllByPage(HttpServletRequest request) {
		Map<String, Object> reqMap = WebKit.getParams(request);
		WebKit.setPage(WebKit.toPageIndex(reqMap), WebKit.toPageSize(reqMap));
		List<App> list = appService.findAll(reqMap);
		PageInfo<App> pages = new PageInfo<App>(list);
		Result result = Result.success();
		result.setResData(pages.getList(), pages.getTotal());
		return result;
	}

	/**
	 * 添加或者修改 主键id不为空时更新,否则保存
	 */
	@KLog("app管理->添加或修改app")
	@RequiresPermissions({ "sys:app:create", "sys:app:update" })
	@ResponseBody
	@RequestMapping(value = "/saveORupdate", method = RequestMethod.POST)
	public Result saveORupdate(App app) {
		if (app == null) {
			return Result.error(ExceptionEnum.PARAM_NOT_NULL);
		}
		if (app.getId() == null) {
			app.setCreateDate(new Date());
			app.setCreator(getUserId());
			return appService.save(app) ? Result.success("保存信息成功!") : Result.error("保存信息失败!");
		} else {
			app.setUpdateDate(new Date());
			app.setUpdater(getUserId());
			return appService.updateById(app) ? Result.success("更新信息成功!") : Result.error("更新信息失败!");
		}
	}
}
