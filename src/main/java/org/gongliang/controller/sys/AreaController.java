package org.gongliang.controller.sys;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.gongliang.common.base.BaseController;
import org.gongliang.common.base.Result;
import org.gongliang.entity.sys.Area;
import org.gongliang.kit.web.WebKit;
import org.gongliang.service.sys.AreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import springfox.documentation.annotations.ApiIgnore;

/**
* @desc   控制器
* @author 龚亮
* @version 1.0
* @date 2017年05月15日 02:52:17
*/
@Controller
@RequestMapping("/sys_area")
@ApiIgnore
public class AreaController extends BaseController {

    @Autowired
    private AreaService areaService;

	@ResponseBody
	@RequestMapping(value = "/findAll", method = { RequestMethod.GET, RequestMethod.POST })
	public Result findAll(HttpServletRequest request) {
		Map<String, Object> reqMap = WebKit.getParams(request);
		List<Area> list = areaService.findAll(reqMap);
		Result result = Result.success();
		result.setResData(list, list.size() + 0L);
		return result;
	}

	@RequiresPermissions("sys:area:view")
	@GetMapping("/main")
	public String main() {
		return "sys/Area/main";
	}

}
