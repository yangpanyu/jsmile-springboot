### 项目说明


jsmile-springboot是一个基于springboot微服务框架的权限管理系统。参考了git一些开源的后台系统，比如renren，guns。
*谨慎使用devtools热加载，由于devtool热加载会创建一个新的 ClaassLoader 进行加载class文件，会出现A instanceof A==false的问题

### 体验地址
http://123.207.187.145:8080/springboot/      用户名： admin  密码：123456

### 技术选型：

- 核心框架：Spring boot
- 安全框架：Apache Shiro 
- 视图框架：freemarker
- 持久层框架：MyBatis 
- 定时器：Quartz 
- 数据库连接池：Druid 
- 日志管理：SLF4J 1.7、Log4j
- 页面交互：easyui
- 接口文档管理: Swagger2
- 图标: iconfont
- 文件上传: dropzone，ueditor

### 软件需求

- JDK1.7+
- MySQL5.5+
- Tomcat7.0+
- Maven3.0+
- nginx(必须配nginx，否则dropzone，ueditor上传模块无效)
- redis（缓存与ehcache二选一。可自行配置）

### jsmile理念
  无聊的代码，毁掉就好。 --微笑的面对java开发!

### 项目截图
![输入图片说明](https://git.oschina.net/uploads/images/2017/0622/160159_891bd10a_376262.png "在这里输入图片标题")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0622/160620_4bdf9886_376262.png "在这里输入图片标题")

### 技术交流




